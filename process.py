import csv
from collections import defaultdict
from beautifultable import BeautifulTable

# importing csv data
def iterate(iterable):
	iterator = iter(iterable)
	item = iterator.next()

columns = defaultdict(list)
my_list=[]

with open('data.csv') as f:
    reader = csv.DictReader(f) 
    for row in reader:     	
    	my_list.append(row)

    	for (k,v) in row.items(): 
            columns[k].append(v) 

# initializing
gorkha_F=0
morang_F=0
nawalparasi_F=0
parsa_F=0
rasuwa_F=0
sindhupalchok_F=0
gorkha_M=0
morang_M=0
nawalparasi_M=0
parsa_M=0
rasuwa_M=0
sindhupalchok_M=0

# increment 
for i in range(len(my_list)):
	if (my_list[i]['District']=="gorkha"):
		if (my_list[i]['Gender']=="male"):
			gorkha_M+=1
	if (my_list[i]['District']=="morang"):
		if (my_list[i]['Gender']=="male"):
			morang_M+=1
	if (my_list[i]['District']=="nawalparasi"):
		if (my_list[i]['Gender']=="male"):
			nawalparasi_M+=1
	if (my_list[i]['District']=="parsa"):
		if (my_list[i]['Gender']=="male"):
			parsa_M+=1
	if (my_list[i]['District']=="rasuwa"):
		if (my_list[i]['Gender']=="male"):
			rasuwa_M+=1
	if (my_list[i]['District']=="sindhupalchok"):
		if (my_list[i]['Gender']=="male"):
			sindhupalchok_M+=1


	if (my_list[i]['District']=="sindhupalchok"):
		if (my_list[i]['Gender']=="female"):
			sindhupalchok_F+=1
	if (my_list[i]['District']=="rasuwa"):
		if (my_list[i]['Gender']=="female"):
			rasuwa_F+=1
	if (my_list[i]['District']=="parsa"):
		if (my_list[i]['Gender']=="female"):
			parsa_F+=1
	if (my_list[i]['District']=="nawalparasi"):
		if (my_list[i]['Gender']=="female"):
			nawalparasi_F+=1
	if (my_list[i]['District']=="morang"):
		if (my_list[i]['Gender']=="female"):
			morang_F+=1
	if (my_list[i]['District']=="gorkha"):
		if (my_list[i]['Gender']=="female"):
			gorkha_F+=1

# empty list
female=[]
male=[]

# sorting unique district
district=set(columns["District"])
sorted_district=sorted(district)

# assigning value to respective district
for i in range(len(sorted_district)):
	if(sorted_district[i]=="gorkha"):
		female.append(gorkha_F)
		male.append(gorkha_M)
	if(sorted_district[i]=="morang"):
		female.append(morang_F)
		male.append(morang_M)
	if(sorted_district[i]=="nawalparasi"):
		female.append(nawalparasi_F)
		male.append(nawalparasi_M)
	if(sorted_district[i]=="parsa"):
		female.append(parsa_F)
		male.append(parsa_M)
	if(sorted_district[i]=="rasuwa"):
		female.append(rasuwa_F)
		male.append(rasuwa_M)
	if(sorted_district[i]=="sindhupalchok"):
		female.append(sindhupalchok_F)
		male.append(sindhupalchok_M)

# creating table via beautifultable
table = BeautifulTable()
table.column_headers = ["District", "Female", "Male","Total"]
grand_total=[]

def add(a,b):
	gt=a+b
	grand_total.append(gt)
	return gt

def total(s):
	return sum(s)

for i in range(6):
	table.append_row([sorted_district[i], female[i], male[i],add(female[i], male[i])])
table.append_row(["Total",total(female),total(male),total(grand_total)])
print(table)

